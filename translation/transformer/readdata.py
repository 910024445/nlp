from config import *
from data import loaddata, MyDataset
dataset = torch.load(DATA_ROOT + "dataset{}".format(num_examples))
eng = torch.load(DATA_ROOT + "eng_vocab{}".format(num_examples))
zh = torch.load(DATA_ROOT + "zh_vocab{}".format(num_examples))
print(len(eng))
train_iter = loaddata(dataset, 64)

for X, X_valid_len, Y, Y_valid_len in train_iter:
    print('X:', X.type(torch.int32))
    print('X的有效长度:', X_valid_len)
    print('Y:', Y.type(torch.int32))
    print('Y的有效长度:', Y_valid_len)
    break

