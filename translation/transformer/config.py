import torch

DATA_ROOT = '../../data/eng-cmn/' # the parent root where your train/val/test data are stored
MODEL_ROOT = '../../model/translation/' # the root to buffer your checkpoints

special_symbols = ['<unk>', '<pad>', '<bos>', '<eos>']

MAX_LENGTH = 10
LR = lr = 0.002 # initial LR

device = DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

# num_steps输入为句子长度
# 为实际句子长度+1 (inc中的eos和dec中的bos)
num_hiddens, num_layers, dropout, batch_size, num_steps = 64, 3, 0.1, 64, 10

ffn_num_input, ffn_num_hiddens, num_heads = 64, 128, 2
key_size, query_size, value_size = 32, 32, 32
norm_shape = [64] # 即hidden维数
num_epochs = 120
num_examples = ""
