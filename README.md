# NLP

### 介绍
NLP项目，主要包括atten-seq2seq和transformer模型，实现机器翻译以及对话系统。

### 软件架构
软件架构说明


### 安装教程

1.  pip install -r requirements.txt
2.  可能安装完后会提示前面两个东西版本不符合，不用管他
3.  transformer对话系统标准模型，在dailydialog训练对话系统模型，链接：https://pan.xunlei.com/s/VN8UfYM4o2Ef4r5hAHYm8KmUA1?pwd=5rht#
提取码：5rht
4.  transformer机器翻译的模型就在model/translation文件夹里面。
5.  transformer机器翻译的模型是用cuda:1训练的，如果要使用可能需要修改代码
    
    如：gpu->cpu，即在CPU上使用

    torch.load('trans_encoder.mdl', map_location= lambda storage, loc: storage)

    torch.load('trans_decoder.mdl', map_location= lambda storage, loc: storage)

    如：cuda1->cuda0，即在gpu上使用

    torch.load('trans_encoder.mdl', map_location={'cuda:1' : 'cuda:0'})

    torch.load('trans_decoder.mdl', map_location={'cuda:1' : 'cuda:0'})

### 运行

安装完python环境后
对应修改config.py的路径
然后运行translation/transformer/predict.py即可


### 使用说明

1.  分对话系统和机器翻译两部分
2.  data为数据集
3.  model为训练的模型
4.  translation文件夹下又分了Seq2Seq和transformer两个模型，大家按需查看使用
5.  以transformer文件夹为例，attention.py主要实现了注意力机制，transformer.py实现了transformer的主体架构，data.py为数据的预处理以及生成了词典、dataset、dataloader，readdata.py运行可以查看数据形状，train.py为训练模型，predict.py为预测，config.py为一些参数的定义。
6.  transformer机器翻译的模型是用cuda:1训练的，如果要使用可能需要修改代码
    
    如：gpu->cpu，即在CPU上使用

    torch.load('trans_encoder.mdl', map_location= lambda storage, loc: storage)

    torch.load('trans_decoder.mdl', map_location= lambda storage, loc: storage)

    如：cuda1->cuda0，即在gpu上使用

    torch.load('trans_encoder.mdl', map_location={'cuda:1' : 'cuda:0'})

    torch.load('trans_decoder.mdl', map_location={'cuda:1' : 'cuda:0'})

### 结果

##### base transformer:
1. 训练集loss 1.040, PPL 2.835
2. 验证集loss 2.768, PPL 17.635 
3. BLEU1-4: [0.0773, 0.0376, 0.0104, 0.0053]
4. distinct1-3: [0.02798, 0.15453, 0.36269]

##### schedule sampling  tg=0.9, tr=0.95
1. 训练集loss 0.930, PPL 2.538
2. 验证集loss 3.289, PPL 29.728
3. BLEU1-4: [0.0918, 0.0479, 0.0183, 0.0117] 
4. distinct1-3:  [0.029054, 0.18474, 0.43019]

### 各指标说明
1. loss为cross_entropy_loss，mean(-log(softmax(NLL(X))))
2. ppl：根据每个词来估计一句话出现的概率  ppl=exp(loss)
3. bleu：![img.png](img.png)
4. distinct：不重复数除以总数，以全部结果作测试