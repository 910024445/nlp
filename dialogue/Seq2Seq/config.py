import torch

# http://yanran.li/dailydialog
DATA_ROOT = '../../data/ijcnlp_dailydialog/' # the parent root where your train/val/test data are stored
MODEL_ROOT = '../../model/dialog/' # the root to buffer your checkpoints
# LOG_ROOT = '../log/' # the root to log your train/val status
SOS_token = 0
EOS_token = 1

MAX_LENGTH = 20
EMBEDDING_SIZE = 300 # feature dimension
LR = 0.01 # initial LR
# NUM_EPOCH = 125 # total epoch number (use the firt 1/25 epochs to warm up)
# WEIGHT_DECAY = 5e-4 # do not apply to batch_norm parameters
# MOMENTUM = 0.9
# STAGES = [35, 65, 95] # epoch stages to decay learning rate
DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
